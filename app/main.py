from typing import Annotated
from fastapi import FastAPI, HTTPException, Body
from fastapi.responses import FileResponse
import subprocess
from config import child_args, output_file
import time
import os
SC_CLK_TCK = os.sysconf('SC_CLK_TCK')
PAGESIZE = os.sysconf('SC_PAGESIZE')

app = FastAPI(docs_url='/api/docs')
child = None
outfile = None

def start_process():
    global child, outfile
    if child and not child.poll():
        return # already alive
    if outfile:
        outfile.close()
    outfile = open(output_file, 'wb')
    child = subprocess.Popen(child_args, stdin=subprocess.DEVNULL, stdout=outfile, stderr=outfile)

def stop_process():
    global child
    if not child or child.poll():
        return # already dead
    child.terminate()

def kill_process():
    global child
    if not child or child.poll():
        return # already dead
    child.kill()

@app.post('/api/process')
def post_process(data: Annotated[str, Body()]):
    if data == 'start':
        start_process()
        return 'started'
    elif data == 'stop':
        stop_process()
        return 'stopped'
    elif data == 'kill':
        kill_process()
        return 'killed'
    else:
        raise HTTPException(status_code=400, detail='Unknown action')


@app.get('/api/process')
def get_process():
    if child and not child.poll():
        return 'alive'
    else:
        return 'dead'
    

@app.get('/api/process/result')
def get_process_result():
    if outfile: # FileResponse 500s instead of 404 if the file doesn't exist, so we need to check
        return FileResponse(output_file)
    else:
        raise HTTPException(status_code=404)


@app.get('/api/process/metrics')
def get_process_metrics():
    if not child or child.poll():
        return {'status': 'dead'}
    # if the process died between poll() and open() the zombie is still alive
    stat = open(f'/proc/{child.pid}/stat', 'rb').read()
    stat = stat[stat.rfind(b')')+2:].decode().split(' ')
    ret = {'status': 'alive'}
    ret['user_time'] = int(stat[11]) / SC_CLK_TCK
    ret['system_time'] = int(stat[12]) / SC_CLK_TCK
    ret['real_time'] = time.clock_gettime(time.CLOCK_BOOTTIME) - int(stat[19]) / SC_CLK_TCK
    ret['rss_mb'] = int(stat[21]) * PAGESIZE / 1048576
    return ret
